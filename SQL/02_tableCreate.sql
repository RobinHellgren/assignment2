USE SuperheroesDb;

CREATE TABLE superhero (
	superhero_id int PRIMARY KEY IDENTITY(1,1),
	name nvarchar(50) null,
	alias nvarchar(19) null,
	origin nvarchar(20) null
)

CREATE TABLE assistant (
	assistant_id int PRIMARY KEY IDENTITY(1,1),
	name nvarchar(50) null
)

CREATE TABLE power (
	power_id int PRIMARY KEY IDENTITY(1,1),
	name nvarchar(50) null,
	description nvarchar(100) null
)