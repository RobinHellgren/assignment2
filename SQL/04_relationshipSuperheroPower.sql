USE SuperheroesDb;

CREATE TABLE superhero_power (
	power_id int FOREIGN KEY REFERENCES power(power_id),
	superhero_iD int FOREIGN KEY REFERENCES superhero(superhero_id),
	PRIMARY KEY (power_id, superhero_id)
)

