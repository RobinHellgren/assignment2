USE SuperheroesDb;

INSERT INTO superhero (name, alias, origin)
VALUES ('Sara', 'sarso', 'the woods'),
	   ('Wildfire', 'WF', 'the wild'),
	   ('Dark Pearl', 'DF', 'the ocean');
