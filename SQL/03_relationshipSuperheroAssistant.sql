USE SuperheroesDb;

ALTER TABLE assistant
	ADD superhero_id int CONSTRAINT FK_assistant_superhero FOREIGN KEY REFERENCES superhero(superhero_id)