USE SuperheroesDb;

INSERT INTO power (name, description)
VALUES ('Shapeshifting', 'Can shift shape'),
	   ('Super speed', 'Can run super fast'),
	   ('Flying', 'Can fly'),
	   ('Mind control', 'Can control minds');

INSERT INTO superhero_power(superhero_id, power_id)
VALUES (1, 1),
	   (2, 2),
	   (2, 3),
	   (3, 1);
