﻿using System;
using System.Collections.Generic;

namespace assignment_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Environment.SetEnvironmentVariable("Db pass", "Supersecret123");

            DataAccess.CustomerRepository repo = new() { EmployeeRepository = new DataAccess.EmployeeRepository() };



            Models.Customer cust = repo.GetById(11, false);
            cust.Email = "something@adomain.registrar";
            cust.LastName = "Bengtsson";
            repo.UpdateCustomer(cust);

            Console.WriteLine("------------------------------------------------GetById------------------------------------------------");
            Console.WriteLine(repo.GetById(cust.CustomerId, false));
            Console.WriteLine("------------------------------------------------GetAll------------------------------------------------");
            foreach (Models.Customer customer in repo.GetAll(true))
            {
                Console.WriteLine(customer);
            }

            Console.WriteLine("------------------------------------------------GetByName------------------------------------------------");
            foreach (Models.Customer customer in repo.GetByName("Beng", false))
            {
                Console.WriteLine(customer);
            }
            Console.WriteLine("------------------------------------------------GetFrequenzyPerCountry------------------------------------------------");
            Console.WriteLine(repo.GetFrequenzyPerCountry());
            Console.WriteLine("------------------------------------------------GetHighestSpenders------------------------------------------------");
            Console.WriteLine(repo.GetHighestSpenders());
            Console.WriteLine("------------------------------------------------GetMostPopularGenre------------------------------------------------");
            Console.WriteLine(repo.GetMostPopularGenre(cust));
        }
    }
}
