﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.Models
{
    public class CustomerHighestSpenderList
    {
        public Dictionary<FullName, decimal> SpenderList { get; set; } = new();

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine("*******************************************");
            int rowNr = 1;
            foreach (KeyValuePair<FullName, decimal> keyValuePair in SpenderList)
            {
                sb.AppendLine(rowNr + " - " + keyValuePair.Key + ": " + keyValuePair.Value);
                rowNr++;
            }
            sb.AppendLine("*******************************************");
            return sb.ToString();
        }
    }
}
