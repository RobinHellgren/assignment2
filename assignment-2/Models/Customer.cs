﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public Employee SupportRep { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine("##################################################################");
            sb.AppendLine("##################################################################");
            sb.AppendLine("##################################################################");
            sb.AppendLine("ID: " + CustomerId);
            sb.AppendLine("First Name: " + FirstName);
            sb.AppendLine("Last name: " + LastName);
            sb.AppendLine("Company: " + Company);
            sb.AppendLine("Address: " + Address);
            sb.AppendLine("City: " + City);
            sb.AppendLine("State: " + State);
            sb.AppendLine("Country: " + Country);
            sb.AppendLine("Postal code: " + PostalCode);
            sb.AppendLine("Phone: " + Phone);
            sb.AppendLine("Fax: " + Fax);
            sb.AppendLine("Email: " + Email);
            if (SupportRep != null)
            {
                sb.AppendLine("Support Rep: " + SupportRep.FirstName + " " + SupportRep.LastName + " - ID: " + SupportRep.EmployeeId);
            }
            else
            {
                sb.AppendLine("Support Rep: " + "Not loaded");
            }
            sb.AppendLine("##################################################################");
            sb.AppendLine("##################################################################");
            sb.AppendLine("##################################################################");

            return sb.ToString();
        }
    }
}
