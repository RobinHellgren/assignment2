﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.Models
{
    public class CustomerFavoriteGenres
    {
        public List<string> genreList { get; set; } = new();

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine("*******************************************");
            int rowNr = 1;
            foreach (string genre in genreList)
            {
                sb.AppendLine(rowNr + ": " + genre);
                rowNr++;
            }
            sb.AppendLine("*******************************************");

            return sb.ToString();
        }
        
    }
}
