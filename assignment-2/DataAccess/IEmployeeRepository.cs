﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.DataAccess
{
    interface IEmployeeRepository
    {
        public Models.Employee GetById(int id);
    }
}
