﻿using assignment_2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace assignment_2.DataAccess
{
    class CustomerRepository : ICustomerRepository
    {
        public IEmployeeRepository EmployeeRepository {private get; set; }
        /// <summary>
        /// Adds the Customer to the database using sqlclient. Passes the values stored in the Customer object into a sql query and sends it to the database
        /// </summary>
        /// <param name="customer">The customer object that is added to the database</param>
        /// <returns></returns>
        /// <summary>
        /// Runs a sql query which fetches all rows in the customer table from the database using sql client.
        /// </summary>
        /// <returns>A </returns>
        
        public bool AddCustomer(Customer customer)
        {
            bool result = false;
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                connection.Open();
                List<Customer> customerList = new();
                using (SqlCommand command = new("INSERT INTO Customer(FirstName,LastName,Company,Address,City,State,Country," +
                    "PostalCode,Phone,Fax,Email,SupportRepId)" +
                    "VALUES (@firstName, " +
                    "@lastName, " +
                    "@company," +
                    "@address," +
                    "@city," +
                    "@state," +
                    "@country," +
                    "@postalCode," +
                    "@phone," +
                    "@fax," +
                    "@email," +
                    "@supportRepId); SELECT SCOPE_IDENTITY()", connection))
                {
                    command.Parameters.AddWithValue("@firstName", customer.FirstName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@lastName", customer.LastName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@company", customer.Company ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@address", customer.Address ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@city", customer.City ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@state", customer.State ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@country", customer.Country ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@postalCode", customer.PostalCode ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@phone", customer.Phone ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@fax", customer.Fax ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@email", customer.Email ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@supportRepId", (customer.SupportRep != null) ? customer.SupportRep.EmployeeId : DBNull.Value);


                    customer.CustomerId = Convert.ToInt32(command.ExecuteScalar());
                    result = true;
                }
                return result;
            }
        }
        /// <summary>
        /// Runs a sql query which fetches all rows in the customer table from the database using sql client.
        /// </summary>
        /// <param name="lazyLoad">Determines if the support rep should be loaded as well</param>
        /// <returns>List<customer> object containing all rows from the customer table</returns>
        public List<Customer> GetAll(bool lazyLoad)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                connection.Open();
                List<Customer> customerList = new();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Customer", connection))
                {
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Customer customer = new();
                        customer.CustomerId = (int)reader["CustomerId"];
                        customer.FirstName = (!reader.IsDBNull("FirstName")) ? (string)reader["FirstName"] : null;
                        customer.LastName = (!reader.IsDBNull("LastName")) ? (string)reader["LastName"] : null;
                        customer.Company = (!reader.IsDBNull("Company")) ? (string)reader["Company"] : null;
                        customer.Address = (!reader.IsDBNull("Address")) ? (string)reader["Address"] : null;
                        customer.City = (!reader.IsDBNull("City")) ? (string)reader["City"] : null;
                        customer.State = (!reader.IsDBNull("State")) ? (string)reader["State"] : null;
                        customer.Country = (!reader.IsDBNull("Country")) ? (string)reader["Country"] : null;
                        customer.PostalCode = (!reader.IsDBNull("PostalCode")) ? (string)reader["PostalCode"] : null;
                        customer.Phone = (!reader.IsDBNull("Phone")) ? (string)reader["Phone"] : null;
                        customer.Fax = (!reader.IsDBNull("Fax")) ? (string)reader["Fax"] : null;
                        customer.Email = (!reader.IsDBNull("Email")) ? (string)reader["Email"] : null;
                        customer.SupportRep = (!lazyLoad && !reader.IsDBNull("SupportRepId")) ? EmployeeRepository.GetById((int)reader["SupportRepId"]) : null;

                        customerList.Add(customer);
                    }
                }
                return customerList;


            }
        }
        /// <summary>
        /// Fetches the customer with the given id from the database using sql client
        /// </summary>
        /// <param name="id">The id of the customer row that is going to be fetched</param>
        /// <param name="lazyLoad">Determines if the support rep should be loaded as well</param>
        /// <returns>The customer with the given id as a Customer object</returns>
        public Customer GetById(int id, bool lazyLoad)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                Customer result = null;
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Customer WHERE CustomerId=@id", connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Customer customer = new();
                        customer.CustomerId = (int)reader["CustomerId"];
                        customer.FirstName = (!reader.IsDBNull("FirstName")) ? (string)reader["FirstName"] : null;
                        customer.LastName = (!reader.IsDBNull("LastName")) ? (string)reader["LastName"] : null;
                        customer.Company = (!reader.IsDBNull("Company")) ? (string)reader["Company"] : null;
                        customer.Address = (!reader.IsDBNull("Address")) ? (string)reader["Address"] : null;
                        customer.City = (!reader.IsDBNull("City")) ? (string)reader["City"] : null;
                        customer.State = (!reader.IsDBNull("State")) ? (string)reader["State"] : null;
                        customer.Country = (!reader.IsDBNull("Country")) ? (string)reader["Country"] : null;
                        customer.PostalCode = (!reader.IsDBNull("PostalCode")) ? (string)reader["PostalCode"] : null;
                        customer.Phone = (!reader.IsDBNull("Phone")) ? (string)reader["Phone"] : null;
                        customer.Fax = (!reader.IsDBNull("Fax")) ? (string)reader["Fax"] : null;
                        customer.Email = (!reader.IsDBNull("Email")) ? (string)reader["Email"] : null;
                        customer.SupportRep = (!lazyLoad && !reader.IsDBNull("SupportRepId")) ? EmployeeRepository.GetById((int)reader["SupportRepId"]) : null;

                        result = customer;

                    }
                }
                return result;
            }
        }
        /// <summary>
        /// Fetches the customer(s) with the given name from the database using sql client. Also looks for partial matches before and after the parameter.
        /// </summary>
        /// <param name="name">The name string that is searched for in the database</param>
        /// <param name="lazyLoad">Determines if the support rep should be loaded as well</param>
        /// <returns>A list of customer objects that match or partially match the search string</returns>
        public List<Customer> GetByName(string name, bool lazyLoad)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                name = "%" + name + "%";
                List<Customer> results = new();
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Customer WHERE FirstName LIKE @name OR LastName LIKE @name", connection))
                {
                    command.Parameters.AddWithValue("@name", name);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Customer customer = new();
                        customer.CustomerId = (int)reader["CustomerId"];
                        customer.FirstName = (!reader.IsDBNull("FirstName")) ? (string)reader["FirstName"] : null;
                        customer.LastName = (!reader.IsDBNull("LastName")) ? (string)reader["LastName"] : null;
                        customer.Company = (!reader.IsDBNull("Company")) ? (string)reader["Company"] : null;
                        customer.Address = (!reader.IsDBNull("Address")) ? (string)reader["Address"] : null;
                        customer.City = (!reader.IsDBNull("City")) ? (string)reader["City"] : null;
                        customer.State = (!reader.IsDBNull("State")) ? (string)reader["State"] : null;
                        customer.Country = (!reader.IsDBNull("Country")) ? (string)reader["Country"] : null;
                        customer.PostalCode = (!reader.IsDBNull("PostalCode")) ? (string)reader["PostalCode"] : null;
                        customer.Phone = (!reader.IsDBNull("Phone")) ? (string)reader["Phone"] : null;
                        customer.Fax = (!reader.IsDBNull("Fax")) ? (string)reader["Fax"] : null;
                        customer.Email = (!reader.IsDBNull("Email")) ? (string)reader["Email"] : null;
                        customer.SupportRep = (!lazyLoad && !reader.IsDBNull("SupportRepId")) ? EmployeeRepository.GetById((int)reader["SupportRepId"]) : null;

                        results.Add(customer);

                    }
                }
                return results;
            }
        }
        /// <summary>
        /// Fetches a list of countries that customers resides in and orders them in order from most resided in to least resided in. 
        /// </summary>
        /// <returns>A CustomerCountryOfOriginList object containing a ordered list of countries</returns>
        public CustomerCountryOfOriginList GetFrequenzyPerCountry()
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                CustomerCountryOfOriginList results = new();
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT Country, count(Country) AS freq FROM Customer GROUP BY Country ORDER BY freq DESC", connection))
                {

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string country = (string)reader["Country"];
                        int count = (int)reader["freq"];
                        results.CountryList.Add(country, count);
                    }
                }
                return results;
            }
        }
        /// <summary>
        /// Fetches a list of the customers spending totals, ordered from highest to lowest
        /// </summary>
        /// <returns>A CustomerHighestSpenderList object, containing a ordered list of the total spendings of the customers, ordered highest to lowest </returns>
        public CustomerHighestSpenderList GetHighestSpenders()
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                CustomerHighestSpenderList results = new();
                connection.Open();
                using (SqlCommand command = new SqlCommand(
                    "SELECT cust.CustomerId, cust.FirstName, cust.LastName, SUM(invoiceSum.rowSum) AS total " +
                    "FROM Customer cust " +
                    "JOIN " + 
                        "(SELECT Invoice.InvoiceId, Invoice.CustomerId, SUM(UnitPrice) AS rowSum " +
                        "FROM Invoice, InvoiceLine " +
                        "WHERE Invoice.InvoiceId = InvoiceLine.InvoiceId GROUP BY Invoice.InvoiceId, Invoice.CustomerId) invoiceSum " +
                    "ON cust.CustomerId = invoiceSum.CustomerId " +
                    "GROUP BY invoiceSum.CustomerId, cust.CustomerId, cust.FirstName, cust.LastName " +
                    "ORDER BY total DESC", connection))
                {

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        FullName name = new();
                        name.FirstName = (string)reader["FirstName"];
                        name.LastName = (string)reader["LastName"];
                        decimal total = (decimal)reader["total"];
                        results.SpenderList.Add(name,total);
                    }
                }
                return results;
            }
        }
        /// <summary>
        /// Fetches the most frequently listened to genre for the given customer, returns more than one genre in case of ties.
        /// </summary>
        /// <param name="customer">The customer that is being queried for more popular genre</param>
        /// <returns>A CustomerFavoriteGenres object containing a list of most popular genre, more than one in case of ties</returns>
        public CustomerFavoriteGenres GetMostPopularGenre(Customer customer)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                CustomerFavoriteGenres results = new();
                connection.Open();
                using (SqlCommand command = new SqlCommand(
                    "SELECT genreDictonary.genreName, count(genreDictonary.genreId) as genreFreq FROM" +
                        "(SELECT t.TrackId AS track, g.GenreId AS genreId, g.Name AS genreName " +
                        "FROM Track t, Genre g " +
                        "WHERE t.GenreId = g.GenreId) genreDictonary " +
                    "JOIN" +
                        "(SELECT Invoice.CustomerId, InvoiceLine.TrackId AS track " +
                        "FROM Invoice, InvoiceLine " +
                        "WHERE Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "GROUP BY Invoice.CustomerId, Invoice.InvoiceId, InvoiceLine.TrackId) invoiceList " +
                    "ON genreDictonary.track = invoiceList.track " +
                    "WHERE invoiceList.CustomerId = @customerId " +
                    "GROUP BY genreDictonary.genreId, genreDictonary.genreName " +
                    "ORDER BY count(genreDictonary.genreId)  DESC", connection))
                {
                    command.Parameters.AddWithValue("@customerId", customer.CustomerId);

                    SqlDataReader reader = command.ExecuteReader();

                    List<KeyValuePair<string,int>> allResults = new();

                    while (reader.Read())
                    {
                        string genreName = (string)reader["genreName"];
                        int freq = (int)reader["genreFreq"];
                        allResults.Add(new KeyValuePair<string, int>(genreName,freq));
                    }
                    
                    foreach (KeyValuePair<string, int> row in allResults)
                    {
                        if(row.Value == allResults[0].Value)
                        {
                            results.genreList.Add(row.Key);
                        }
                    }
                }
                return results;
            }
        }
        /// <summary>
        /// Fetches a subsection of the full customer list determined by the offset and limit parameters
        /// </summary>
        /// <param name="lazyLoad">Determines if the support rep should be loaded as well</param>
        /// <param name="limit">The number of rows that is fetched</param>
        /// <param name="offset">The number of rows that are skipped, starting from the top of the list</param>
        /// <returns></returns>
        public List<Customer> GetPage(int limit, int offset, bool lazyLoad)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                List<Customer> results = new();
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY", connection))
                {
                    command.Parameters.AddWithValue("@limit", limit);
                    command.Parameters.AddWithValue("@offset", offset);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Customer customer = new();
                        customer.CustomerId = (int)reader["CustomerId"];
                        customer.FirstName = (!reader.IsDBNull("FirstName")) ? (string)reader["FirstName"] : null;
                        customer.LastName = (!reader.IsDBNull("LastName")) ? (string)reader["LastName"] : null;
                        customer.Company = (!reader.IsDBNull("Company")) ? (string)reader["Company"] : null;
                        customer.Address = (!reader.IsDBNull("Address")) ? (string)reader["Address"] : null;
                        customer.City = (!reader.IsDBNull("City")) ? (string)reader["City"] : null;
                        customer.State = (!reader.IsDBNull("State")) ? (string)reader["State"] : null;
                        customer.Country = (!reader.IsDBNull("Country")) ? (string)reader["Country"] : null;
                        customer.PostalCode = (!reader.IsDBNull("PostalCode")) ? (string)reader["PostalCode"] : null;
                        customer.Phone = (!reader.IsDBNull("Phone")) ? (string)reader["Phone"] : null;
                        customer.Fax = (!reader.IsDBNull("Fax")) ? (string)reader["Fax"] : null;
                        customer.Email = (!reader.IsDBNull("Email")) ? (string)reader["Email"] : null;
                        customer.SupportRep = (!lazyLoad && !reader.IsDBNull("SupportRepId")) ? EmployeeRepository.GetById((int)reader["SupportRepId"]) : null;

                        results.Add(customer);

                    }
                }
                return results;
            }
        }
        /// <summary>
        /// Updates the provided Customer objects representation in the database
        /// </summary>
        /// <param name="customer">The customer object containing the new values</param>
        /// <returns>True if the operation is successful, otherwise false</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool result = false;
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                connection.Open();
                using (SqlCommand command = new("UPDATE Customer " +
                    "SET FirstName = @firstName, " +
                    "LastName = @lastName, " +
                    "Company = @company," +
                    "Address = @address," +
                    "City = @city," +
                    "State = @state," +
                    "Country = @country," +
                    "PostalCode = @postalCode," +
                    "Phone = @phone," +
                    "Fax = @fax," +
                    "Email = @email," +
                    "SupportRepId = @supportRepId " +
                    "WHERE CustomerId = @customerId", connection))
                {
                    command.Parameters.AddWithValue("customerId", customer.CustomerId);

                    command.Parameters.AddWithValue("@firstName", customer.FirstName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@lastName", customer.LastName ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@company", customer.Company ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@address", customer.Address ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@city", customer.City ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@state", customer.State ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@country", customer.Country ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@postalCode", customer.PostalCode ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@phone", customer.Phone ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@fax", customer.Fax ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@email", customer.Email ?? (object)DBNull.Value);
                    command.Parameters.AddWithValue("@supportRepId", (customer.SupportRep != null) ? customer.SupportRep.EmployeeId : DBNull.Value);

                    command.ExecuteNonQuery();
                    result = true;
                }
                return result;
            }
        }
    }
}

