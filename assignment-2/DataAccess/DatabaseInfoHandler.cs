﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.DataAccess
{
    public static class DatabaseInfoHandler
    {
        /// <summary>
        /// Builds the SqlCredential object needed to authenticate to the sql server
        /// </summary>
        /// <returns>A SqlCredential object used to connect to the databse</returns>
        public static SqlCredential generateCredentials()
        {
            System.Security.SecureString credentials = new();
            char[] pass = Environment.GetEnvironmentVariable("Db pass").ToCharArray();
            foreach (char letter in pass)
            {
                credentials.AppendChar(letter);
            }
            credentials.MakeReadOnly();
            return new SqlCredential("sa", credentials);
        }
        /// <summary>
        /// Creates the connection string needed to connect to the database
        /// </summary>
        /// <returns>The connection string for the database as a string</returns>
        public static string getConnectionString()
        {
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = "localhost";
            builder.InitialCatalog = "Chinook";
            return builder.ConnectionString;
        }
    }
}
