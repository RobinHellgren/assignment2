﻿using assignment_2.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.DataAccess
{
    class EmployeeRepository : IEmployeeRepository
    {
        /// <summary>
        /// Fetches the employee with the given id from the database.
        /// </summary>
        /// <param name="id">Id of the employee that is being fetched from the database</param>
        /// <returns>The employee with the given id as a Employee object</returns>
        public Employee GetById(int id)
        {
            using (SqlConnection connection = new SqlConnection(DatabaseInfoHandler.getConnectionString(), DatabaseInfoHandler.generateCredentials()))
            {
                Employee result = null;
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM Employee WHERE EmployeeId=@id", connection))
                {
                    command.Parameters.AddWithValue("@id", id);

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Employee employee = new();
                        employee.EmployeeId = (int)reader["EmployeeId"];
                        employee.FirstName = (!reader.IsDBNull("FirstName")) ? (string)reader["FirstName"] : null;
                        employee.LastName = (!reader.IsDBNull("LastName")) ? (string)reader["LastName"] : null;
                        employee.Title = (!reader.IsDBNull("Title")) ? (string)reader["Title"] : null;
                        employee.Address = (!reader.IsDBNull("Address")) ? (string)reader["Address"] : null;
                        employee.City = (!reader.IsDBNull("City")) ? (string)reader["City"] : null;
                        employee.State = (!reader.IsDBNull("State")) ? (string)reader["State"] : null;
                        employee.Country = (!reader.IsDBNull("Country")) ? (string)reader["Country"] : null;
                        employee.PostalCode = (!reader.IsDBNull("PostalCode")) ? (string)reader["PostalCode"] : null;
                        employee.Phone = (!reader.IsDBNull("Phone")) ? (string)reader["Phone"] : null;
                        employee.Fax = (!reader.IsDBNull("Fax")) ? (string)reader["Fax"] : null;
                        employee.Email = (!reader.IsDBNull("Email")) ? (string)reader["Email"] : null;
                        employee.BirthDate = (!reader.IsDBNull("BirthDate")) ? (DateTime)reader["BirthDate"] : DateTime.UnixEpoch;
                        employee.HireDate = (!reader.IsDBNull("HireDate")) ? (DateTime)reader["HireDate"] : DateTime.UnixEpoch;
                        employee.ReportsToId = (!reader.IsDBNull("ReportsTo")) ? (int)reader["ReportsTo"] : 0;
                        result = employee;
                    }
                }
                return result;
            }
        }

    }

}

