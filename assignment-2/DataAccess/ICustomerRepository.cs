﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_2.DataAccess
{
    public interface ICustomerRepository
    {
        public List<Models.Customer> GetAll(bool lazyLoad);
        public Models.Customer GetById(int id, bool lazyLoad);
        public List<Models.Customer> GetByName(string name, bool lazyLoad);
        public List<Models.Customer> GetPage(int limit, int offset, bool lazyLoad);
        public Models.CustomerCountryOfOriginList GetFrequenzyPerCountry();
        public Models.CustomerHighestSpenderList GetHighestSpenders();
        public Models.CustomerFavoriteGenres GetMostPopularGenre(Models.Customer customer);
        public bool AddCustomer(Models.Customer customer);
        public bool UpdateCustomer(Models.Customer customer);

    }
}
